CREATE TABLE medico
  (
     idMedico      NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1
     MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20
     NOORDER NOCYCLE NOT NULL ENABLE,
     idMedicoSupervisor NUMBER(5,0) NOT NULL,
     nombreM        VARCHAR2(20) NOT NULL,
     maternoM       VARCHAR2(20) NOT NULL,
     paternoM       VARCHAR2(20) NOT NULL,
     calleM         VARCHAR2(20) NOT NULL,
     numeroM        NUMBER(5,0) NOT NULL,
     estadoM        VARCHAR(40) NOT NULL, 
     ciudadM        VARCHAR(40) NOT NULL,
     cpM            VARCHAR(20) NOT NULL,
     CURPM          VARCHAR(20) NOT NULL,
    
     CONSTRAINT idmedico_PK PRIMARY KEY (idMedico)
  );
 


CREATE TABLE paciente
  (
     idPaciente      NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1
     MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20
     NOORDER NOCYCLE NOT NULL ENABLE,
     nombreP        VARCHAR2(20) NOT NULL,
     maternoP       VARCHAR2(20) NOT NULL,
     paternoP       VARCHAR2(20) NOT NULL,
     calleP         VARCHAR2(20) NOT NULL,
     numeroP        NUMBER(5,0) NOT NULL,    
     estadoP      VARCHAR2(50) NOT NULL, 
     ciudadP       VARCHAR2(50) NOT NULL,
     cpP            VARCHAR(20) NOT NULL,
     CURPP          VARCHAR(20) NOT NULL,
     FechaNacimientoP DATE NOT NULL,
    
     CONSTRAINT idpaciente_PK PRIMARY KEY (idPaciente)
  ); 
 


CREATE TABLE especialidad
  (
    idEspecialidad      NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1
     MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20
     NOORDER NOCYCLE NOT NULL ENABLE, 
    nombreEspecialidad  VARCHAR(40),
   
    CONSTRAINT idespecialidad_PK PRIMARY KEY (idEspecialidad)
  );



CREATE TABLE medicoIngresarPaciente
  (
     idMedico    NUMBER,
     idPaciente NUMBER,
     numIngreso NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1
     MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20
     NOORDER NOCYCLE NOT NULL ENABLE,
     FechaIngreso DATE NOT NULL,
     cama VARCHAR(20) NOT NULL,
     habitacion NUMBER NOT NULL,
     CONSTRAINT medicoIngresarPacienteIdMedico_FK FOREIGN KEY (idMedico) REFERENCES medico (idMedico),
     CONSTRAINT medicoIngresarPacienteIdPaciente_FK FOREIGN KEY (idPaciente) REFERENCES paciente (idPaciente)
  );

ALTER TABLE medicoIngresarPaciente ADD CONSTRAINT medicoIngresarPaciente_PK PRIMARY KEY (idMedico, idPaciente);






CREATE TABLE medicoConsultarPaciente
  (
     idMedico    NUMBER,
     idPaciente NUMBER,
     numConsulta NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1
     MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20
     NOORDER NOCYCLE NOT NULL ENABLE,
     FechaConsulta DATE NOT NULL,
     consultorio NUMBER NOT NULL,
     CONSTRAINT medicoConsultarPacienteIdMedico_FK FOREIGN KEY (idMedico) REFERENCES medico (idMedico),
     CONSTRAINT medicoConsultarPacienteIdPaciente_FK FOREIGN KEY (idPaciente) REFERENCES paciente (idPaciente)
  );

ALTER TABLE medicoConsultarPaciente ADD CONSTRAINT medicoConsultarPaciente_PK PRIMARY KEY (idMedico, idPaciente);



CREATE TABLE medicoTenerEspecialidad
  (
     idMedico    NUMBER,
     idEspecialidad NUMBER,
     CONSTRAINT medicoTenerEspecialidadIdMedico_FK FOREIGN KEY (idMedico) REFERENCES medico (idMedico),
     CONSTRAINT medicoTenerEspecialidadIdEspecialidad_FK FOREIGN KEY (idEspecialidad) REFERENCES especialidad (idEspecialidad)
  );

ALTER TABLE medicoTenerEspecialidad ADD CONSTRAINT medicoTenerEspecialidad_PK PRIMARY KEY (idMedico, idEspecialidad)


--DROP TABLE medicoIngresarPaciente PURGE
--DROP TABLE medicoConsultarPaciente PURGE
--DROP TABLE medicoTenerEspecialidad PURGE
--DROP TABLE medico PURGE
--DROP TABLE paciente PURGE
--DROP TABLE especialidad PURGE
